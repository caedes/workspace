export default {
  primary: { name: 'Roboto', googleFont: true, styles: ['400', '700'] },
  secondary: { name: 'Comfortaa', googleFont: true, styles: ['400', '700'] },
};
