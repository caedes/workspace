import createTheme from 'spectacle/lib/themes/default';
import colors from './colors';
import fonts from './fonts';

export default createTheme(colors, fonts);
