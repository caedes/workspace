# spectacle-theme-sober

Sober theme for Spectacle

## Installation

```shell
yarn add spectacle-theme-sober
```

## Usage

```javascript
import createTheme from 'spectacle-theme-sober';

const theme = createTheme();
```
