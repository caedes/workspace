export default [
  'présente',
  'actuelle',
  'générale',
  'induite',
  'conjoncturelle',
  'observée',
  'contextuelle',
  'de ces derniers temps',
  "de l'époque actuelle",
  'intrinsèque',
  'que nous constatons',
];
