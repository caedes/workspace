import adjectives from './data/adjectives';
import adverbs from './data/adverbs';
import infinitives from './data/infinitives';
import names from './data/names';
import prepositions from './data/prepositions';
import pronouns from './data/pronouns';
import solutionAdjectives from './data/solution-adjectives';
import solutionNames from './data/solution-names';
import verbs from './data/verbs';

export default [
  prepositions,
  names,
  adjectives,
  ',',
  verbs,
  infinitives,
  pronouns,
  solutionNames,
  solutionAdjectives,
  adverbs,
  '.',
];
