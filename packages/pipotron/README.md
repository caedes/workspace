# pipotron

Pipotron, the french random sentence generator

## Installation

```shell
yarn add pipotron
```

## Usage

```javascript
import pipotron from 'pipotron';

pipotron(); // Considérant l'ambiance observée, il faut de toute urgence revoir chacune des modalités de bon sens à court terme.
```
